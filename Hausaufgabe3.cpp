#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class Domino {
    public:
        Domino( int a, int b );
        void print();
        int a;
        int b;
};

Domino::Domino( int a, int b ) {
    this->a = a;
    this->b = b;
}

void Domino::print() {
    cout << "[" << this->a << ":" << this->b << "]";
}

class DominoListItem {
    public:
        DominoListItem();
        Domino * domino;
        DominoListItem * next;
};

DominoListItem::DominoListItem() {
    this->domino = 0;
    this->next = 0;
}


DominoListItem * readDominos( string filename ){

    ifstream file;  //nutzen von ifstream zum einlesen
    file.open( filename.c_str(), ios::in );

    //erste Zahl ist l�nge der kette
    int len;
    file >> len;
    // cout << "Laenge: " << len << endl;

    int a, b;   //Variablen f�r die Zwischenspiecherung der zwei Zahlen
    Domino * domino;
    DominoListItem * head = 0;
    DominoListItem * last = 0;
    DominoListItem * current = 0;

    //solange Zahlen verf�gbar soll neuer Stein gemacht werden
    while ( file >> a >> b ) {
        //cout << a << ":" << b << endl;

        domino = new Domino( a, b );
        current = new DominoListItem();
        current->domino = domino;

        if ( head == 0 ) {
            head = current;
        }

        if ( last != 0 ) {
            last->next = current;
        }

        last = current;
    }

    return head;
}

void printDominos ( DominoListItem * head ) {
    cout << "Liste" << endl;
    DominoListItem * current = head;
    while( current != 0 ) {
        cout << " ";
        current->domino->print();
        current = current->next;
    }
    cout << endl;
}

int main(int argc, char* argv[])
{
    string filename = "beispiel1.txt";
    DominoListItem * head = readDominos( filename );
    printDominos( head );
    return 0;
}
